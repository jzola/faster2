/***
 *  $Id$
 **
 *  File: CompactFilter.hpp
 *  Created: Sep 09, 2014
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2014 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef COMPACT_FILTER_HPP
#define COMPACT_FILTER_HPP

#include <string>
#include <unordered_set>
#include <vector>

#include "AbstractFilter.hpp"


class CompactFilter : public Filter {
public:
    explicit CompactFilter(const std::vector<std::string>& args)
        : Filter("CompactFilter") {
        if (args.empty() == false) error = "incorrect arguments";
    } // CompactFilter

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        std::unordered_set<std::string> ref;
        for (auto it = db.begin(); it != db.end(); ) {
            auto res = ref.insert(it->name);
            if (res.second == false) it = db.erase(it); else ++it;
        }
        return { true, "" };
    } // run

    std::string help() const {
        return "compact\nExample: ./faster2 . compact, print\nRemove sequences with duplicate names. If two sequences have the same name only the first occurrence will be retained. Note that this filter does not compare the actual sequences.";
    } // help

}; // class CompactFilter

#endif // COMPACT_FILTER_HPP
