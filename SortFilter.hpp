/***
 *  $Id$
 **
 *  File: SortFilter.hpp
 *  Created: Sep 18, 2015
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2015 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef SORT_FILTER_HPP
#define SORT_FILTER_HPP

#include "AbstractFilter.hpp"
#include <string>


class SortFilter : public Filter {
public:
    enum key_type { POS = 0, NAME = 1, SIZE = 2 };

    struct for_access {
        bool operator()(const db_entry& lhs, const db_entry& rhs) const {
            return (lhs.file < rhs.file) || ((lhs.pos < rhs.pos) && !(rhs.file < lhs.file));
        }
    }; // struct for_access

    struct for_name {
        for_name(int ap, int al) : pos(ap), len(al) { }

        bool operator()(const db_entry& lhs, const db_entry& rhs) const {
            auto lpos = (pos >= 0) ? pos : std::max<int>(0, lhs.name.size() + pos);
            auto rpos = (pos >= 0) ? pos : std::max<int>(0, rhs.name.size() + pos);
            auto llen = std::min<int>(lhs.name.size() - lpos, len);
            auto rlen = std::min<int>(rhs.name.size() - rpos, len);
            return (lhs.name.compare(lpos, llen, rhs.name, rpos, rlen) < 0);
        }

        int pos;
        int len;
    }; // struct for_name

    struct for_size {
        bool operator()(const db_entry& lhs, const db_entry& rhs) const {
            return (lhs.size < rhs.size);
        }
    }; // struct for_size


    explicit SortFilter(const std::vector<std::string>& args)
        : Filter("SortFilter") {
        if (args.empty()) key_ = POS;
        else {
            if (args[0] == "name") {
                key_ = NAME;
                if (args.size() == 3) {
                    namep_ = std::stoi(args[1]);
                    namel_ = std::stoi(args[2]);
                    if (namel_ < 1) error = "incorrect name key size";
                }
            } else if (args[0] == "size") key_ = SIZE; else error = "incorrect key";
        }
    } // SortFilter

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        switch (key_) {
          case NAME:
              std::sort(db.begin(), db.end(), for_name(namep_, namel_));
              break;
          case SIZE:
              std::sort(db.begin(), db.end(), for_size());
              break;
          default:
              std::sort(db.begin(), db.end(), for_access());
        }
        return { true, "" };
    } // run

    std::string help() const {
        return "sort [name <pos len> | size]\nExample: ./faster2 . sort name 2 10, print\nSort sequences using specific key. If key is not specified, filename becomes the primary key, and position in the file the secondary key.";
    } // help


private:
    key_type key_ = POS;
    int namep_ = 0;
    int namel_ = std::numeric_limits<int>::max();

}; // class SortFilter

#endif // SORT_FILTER_HPP
