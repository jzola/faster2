/***
 *  $Id$
 **
 *  File: NanoSimFilter.hpp
 *  Created: Jan 22, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef NANOSIM_FILTER_HPP
#define NANOSIM_FILTER_HPP

#include "AbstractFilter.hpp"


class NanoSimFilter : public Filter {
public:
    explicit NanoSimFilter(const std::vector<std::string>& args)
        : Filter("NanoSimFilter"), name_(""), overlap_(0) {
        if (args.size() != 2) error = "incorrect arguments";
        else {
            overlap_ = std::stoi(args[0]);
            name_ = args[1];
        }
        if (overlap_ < 1) error = "too short overlap";
    } // NanoSimFilter

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        std::ofstream n_of(name_ + ".nodes");
        std::ofstream e_of(name_ + ".edges");

        if (db.empty() == true) return { true, "" };

        std::vector<read__> R;

        // analyze read names
        for (int i = 0; i < db.size(); ++i) {
            std::vector<std::string> d;
            read__ r;

            // required in newer versions of NanoSim
            auto fixp = db[i].name.find(";");
            if (fixp < std::string::npos) db[i].name.replace(fixp, 1, "_");

            jaz::split('_', db[i].name, std::back_inserter(d));
            auto di = d.rbegin();

            if (d.size() < 7) return { false, "incorrect NanoSim entry" };

            r.name = db[i].name;
            r.type = *d.begin();
            r.pos = std::stoi(di[6]);
            r.len = db[i].size;
            r.orient = di[3][0]; // FIX ME
            r.ref = i;

            R.push_back(r);
        } // for

        // nodes
        std::sort(std::begin(R), std::end(R), [](const read__& a, const read__& b) {
                return (a.type < b.type) || (!(b.type < a.type) && (a.pos < b.pos)); });
        for (int i = 0; i < R.size(); ++i) n_of << i << " " << R[i].name << "\n";

        // edges
        for (int i = 0; i < R.size() - 1; ++i) {
            for (int j = i + 1; j < R.size(); ++j) {
                // we output: source target overlap source_len target_len source_orient target_orient
                if ((R[i].type != R[j].type) || (R[i].pos + R[i].len - R[j].pos < overlap_)) break;
                e_of << i << " " << j << " " << (R[i].pos + R[i].len - R[j].pos) << " " << R[i].len << " " << R[j].len << " " << R[i].orient << " " << R[j].orient << "\n";
            }
        }

        // reorder database
        std::vector<db_entry> db_new(R.size());
        for (int i = 0; i < db_new.size(); ++i) db_new[i] = db[R[i].ref];
        db = std::move(db_new);

        return { true, "" };
    } // run

    std::string help() const {
        return "";
    } // help


private:
    struct read__ {
        std::string name;
        std::string type;
        int pos;
        int len;
        char orient;
        int ref;
    }; // struct read__

    std::string name_;
    int overlap_;

}; // class NanoSimFilter

#endif // NANOSIM_FILTER_HPP
