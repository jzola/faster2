/***
 *  $Id$
 **
 *  File: SelectFilter.hpp
 *  Created: Jun 21, 2012
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2012-2014 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef SELECT_FILTER_HPP
#define SELECT_FILTER_HPP

#include <fstream>
#include <string>
#include <vector>

#include <jaz/iterator.hpp>


class SelectFilter : public Filter {
public:

    class not_in_args {
    public:
        explicit not_in_args(const std::vector<std::string>& args) : args_(args) { }

        bool operator()(const db_entry& de) const {
            return !std::binary_search(args_.begin(), args_.end(), de.name);
        } // operator()

    private:
        const std::vector<std::string>& args_;

    }; // class not_in_args


    explicit SelectFilter(const std::vector<std::string>& args)
        : Filter("SelectFilter"), args_(args) { }

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        if (args_.empty()) return { false, "empty name list" };

        if (args_[0][0] == '@') {
            std::ifstream f(args_[0].c_str() + 1);
            if (!f) return { false, "failed to open name list" };
            args_.clear();
            jaz::getline_iterator<> it(f), end;
            std::copy(it, end, std::back_inserter(args_));
            f.close();
        }
        std::sort(args_.begin(), args_.end());
        db.erase(std::remove_if(db.begin(), db.end(), not_in_args(args_)), db.end());
        return { true, "" };
    } // run

    std::string help() const {
        return "select '@'<name> | <name> [name1 name2 ...]\nExample: ./faster2 . select \"Seq A\" \"Seq B\"\nExtract sequences that exacly match one of specified names. name, if with prefix @, is a path to the file storing names of sequences to extract (one name per line). Otherwise it is a string representing sequence name. Then, it can contain spaces and any characters permitted in FASTA and FASTQ formats. If a given name contains spaces it should be clearly delimited by double quotes. Note: in versions prior to 0.25 commas are not handled properly! If a given sequence name is listed multiple times it will be extracted only once.";
    } // help


private:
    std::vector<std::string> args_;

}; // class SelectFilter

#endif // SELECT_FILTER_HPP
