/***
 *  $Id$
 **
 *  File: MaphdFilter.hpp
 *  Created: Jan 04, 2016
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef MAPHD_FILTER_HPP
#define MAPHD_FILTER_HPP

#include <cmath>
#include <bio/fastx_iterator.hpp>


class MaphdFilter : public Filter {
public:
    MaphdFilter(const std::string& path, const std::vector<std::string>& args)
        : Filter("MaphdFilter"), dir_(path), args_(args) {
        if (args_.size() < 1) error = "incorrect arguments";

        std::memset(digit_, 0, 256);
        digit_['c'] = digit_['C'] = 1;
        digit_['g'] = digit_['G'] = 2;
        digit_['t'] = digit_['T'] = 3;
        digit_['u'] = digit_['U'] = 3;

    } // MaphdFilter

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        std::ostream* os = &std::cout;
        std::ofstream of;

        int k = std::atoi(args_[0].c_str());
        if ((k < 2) || (k > 8)) return { false, "too high dimension" };

        if ((args_.size() == 2) && (args_[1] != "-")) {
            of.open(args_[1].c_str());
            if (!of) return { false, "failed to create " + args_[1] };
            os = &of;
        }

        if (db.empty() == true) return { true, "" };

        if (db.back().fastq == true) return m_run__<bio::fastq_input_iterator<>>(*os, db, k);
        return m_run__<bio::fasta_input_iterator<>>(*os, db, k);
    } // run

    std::string help() const {
        return "";
    } // help


private:
    void m_map__(const std::string& s, int k, std::vector<int>& L) {
        int l = s.size();
        int end = l - k + 1;

        std::fill(L.begin(), L.end(), 0);
        if (end < 0) return;

        // first kmer
        int v = digit_[s[k - 1]];
        for (int i = 0; i < k - 1; ++i) {
            v += digit_[s[i]] * (1 << ((k - i - 1) << 1));
        }

        if (v < L.size()) L[v]++;

        // and then all other
        int b = 1 << ((k - 1) << 1);

        for (int i = 1; i < end; ++i) {
            v = (v - b * digit_[s[i - 1]]) * 4 + digit_[s[i + k - 1]];
            if (v < L.size()) L[v]++;
        }
    } // m_map__

    template <typename FastaIter>
    std::pair<bool, std::string> m_run__(std::ostream& os, std::vector<db_entry>& db, int k) {
        std::string file = "";
        std::string name = "";

        std::ifstream fs;
        boost::iostreams::filtering_stream<boost::iostreams::input> cs;

        std::istream* is = 0;
        bool is_cs = false;

        unsigned int pos = 0;
        FastaIter fii;

        std::vector<int> L(std::pow(4, k), 0);

        for (auto iter = db.begin(); iter != db.end(); ++iter) {
            if (file != iter->file) {
                file = iter->file;
                name = dir_ + "/" + iter->file;
                is = open_stream(name, fs, cs);
                if (is == 0) return { false, "failed to read " + name };
                is_cs = (is == &cs);
                if (is_cs == true) fii = FastaIter(*is);
                pos = 0;
            }

            if (is_cs == false) {
                is->seekg(iter->offset);
                fii = FastaIter(*is);
            } else {
                std::advance(fii, iter->pos - pos);
                pos = iter->pos;
            }

            if ((is->eof() == false) && (is->fail() == true)) return { false, "stream failed for " + name };

            // here we go
            const std::string& s = std::get<1>(*fii);
            m_map__(s, k, L);

            for (auto& x : L) os << x << " ";
            os << "\n";
        } // for iter

        return { true, "" };
    } // m_run__

    std::string dir_;
    std::vector<std::string> args_;

    char digit_[256];

}; // class MaphdFilter

#endif // MAPHD_FILTER_HPP
