CXX=g++

#BOOST_INCLUDE=-I/usr/local/boost/include
#BOOST_LIB=-L/usr/local/boost/lib

#BOOST_INCLUDE=-I$(BOOST_INC_DIR)
#BOOST_LIB=-L$(BOOST_LIB_DIR)

CXXFLAGS=-std=c++17 -O3 -I. $(BOOST_INCLUDE)

LDLIBS=$(BOOST_LIB) -lboost_iostreams -lboost_filesystem -lboost_system -lz -lbz2

SRC=faster2

all: $(SRC)

clean:
	rm -rf $(SRC)

.cpp:
	$(CXX) $(CXXFLAGS) $< -o $@ $(LDLIBS)
