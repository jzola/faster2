/***
 *  $Id$
 **
 *  File: ReportFilter.hpp
 *  Created: Jun 21, 2012
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2012-2016 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef REPORT_FILTER_HPP
#define REPORT_FILTER_HPP

#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include <jaz/algorithm.hpp>

#include "AbstractFilter.hpp"
#include "index.hpp"


class ReportFilter : public Filter {
public:
    explicit ReportFilter(const std::vector<std::string>& args)
        : Filter("ReportFilter"), args_(args) {
        if (args_.size() > 1) error = "incorrect arguments";
    } // ReportFilter

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        std::ostream* os = &std::cout;
        std::ofstream of;

        if ((args_.size() > 0) && (args_[0] != "-")) {
            of.open(args_[0].c_str());
            if (!of) return { false, "failed to create " + args_[0] };
            os = &of;
        }

        if (db.empty() == true) return { true, "" };

        std::unordered_set<std::string> F;
        for (auto& x : db) F.insert(x.file);

        *os << "total files\t" << F.size() << std::endl;

        *os << "total sequences\t" << db.size() << std::endl;

        *os << "quality scores\t" << (db.back().fastq ? "yes" : "no") << std::endl;

        *os << "clean sequences\t" << std::count_if(db.begin(), db.end(),
                                                    [](const db_entry& de) -> bool {
                                                        return de.hasN == false;
                                                    })
            << std::endl;

        using namespace boost::accumulators;
        accumulator_set<double, stats<tag::sum, tag::min, tag::max, tag::mean, tag::median, tag::variance> > acc;

        for (auto& x : db) acc(x.size);

        *os << "totoal length\t" << static_cast<long long int>(sum(acc)) << std::endl;
        *os << "longest sequence\t" << max(acc) << std::endl;
        *os << "shortest sequence\t" << min(acc) << std::endl;
        *os << "mean length\t" << mean(acc) << std::endl;
        *os << "median length\t" << median(acc) << std::endl;
        *os << "length deviation\t" << std::sqrt(variance(acc)) << std::endl;

        return { true, "" };
    } // run

    std::string help() const {
        return "report\nExample: ./faster2 . report\nGenerate comprehensive report about selected sequences. file specifies the name of the output file, where '-' represents the standard output. If no file name is specified the output is written to the standard output.";
    } // help


private:
    std::vector<std::string> args_;

}; // class ReportFilter

#endif // REPORT_FILTER_HPP
