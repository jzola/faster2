/***
 *  $Id$
 **
 *  File: LengthsFilter.hpp
 *  Created: Nov 15, 2015
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2015 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of faster2.
 */

#ifndef LENGTHS_FILTER_HPP
#define LENGTHS_FILTER_HPP

#include "AbstractFilter.hpp"
#include <map>


class LengthsFilter : public Filter {
public:
    explicit LengthsFilter(const std::vector<std::string>& args)
        : Filter("LengthsFilter"), args_(args) {
        if (args.size() > 1) error = "incorrect arguments";
    } // LengthsFilter

    std::pair<bool, std::string> run(std::vector<db_entry>& db) {
        std::ostream* os = &std::cout;
        std::ofstream of;

        if ((args_.size() > 0) && (args_[0] != "-")) {
            of.open(args_[0].c_str());
            if (!of) return { false, "failed to create " + args_[0] };
            os = &of;
        }

        std::map<int, int> hist;
        for (auto& x : db) hist[x.size]++;
        for (auto& x : hist) *os << x.first << "\t" << x.second << "\n";

        return { true, "" };
    } // run

    std::string help() const {
        return "lengths [file]\nExample: ./faster2 . lengths len.txt\nCreate histogram of sequence length distribution. Histogram is represented as a table in which the first column represents sequence length and the second column is length frequency.";
    } // help


private:
    std::vector<std::string> args_;

}; // class LengthsFilter

#endif // LENGTHS_FILTER_HPP
